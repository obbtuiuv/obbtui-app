import { Component } from '@angular/core';
import { Events, LoadingController, Platform } from 'ionic-angular';
import { GoogleAnalytics } from '@ionic-native/google-analytics';
import { Dialogs } from '@ionic-native/dialogs';
import { EnlacesProvider } from '../../providers/enlaces/enlaces';

@Component({
  selector: 'page-enlaces',
  templateUrl: 'enlaces.html',
})
export class EnlacesPage {
  loading: any;
  enlaces: any;
  enlacesFiltrados: any;
  str_enlace: string;
  str_sinConexion: string;

  constructor(
    public platform: Platform,
    public loadingCtrl: LoadingController,
    public events: Events,
    public enlacesProvider: EnlacesProvider,
    private dialogs: Dialogs,
    private ga: GoogleAnalytics
  ) {
    let env = this;
    let token = localStorage.getItem('token');
    env.loading = env.loadingCtrl.create({ content: 'Cargando...' });
    env.str_sinConexion = 'Tu dispositivo tiene problemas de conexión.';

    // Verificar sesión del dispositivo
    env.events.publish('verificarSesion');

    env.showCargando();
    env.enlacesProvider.getEnlaces(token)
      .subscribe(function(dataEnlaces) {
        env.hideCargando();
        console.log(dataEnlaces);
        if(dataEnlaces.status == 'ERROR') {
          env.alerta(dataEnlaces.error.message);
        } else if(dataEnlaces.status == 'OK') {
          env.enlaces = dataEnlaces.data;
          env.enlacesFiltrados = env.enlaces;
        }
      }, function(err) {
        env.hideCargando();
        env.alerta(env.str_sinConexion);
      });
  }

  ionViewDidLoad() {
    let env = this;
    env.ga.trackView('page-enlaces');
  }

  showCargando() {
    let env = this;
    if(env.loading) {
      env.loading.present();
    } else {
      env.loading = env.loadingCtrl.create({ content: 'Cargando...' });
      env.loading.present();
    }
  }

  hideCargando() {
    let env = this;
    if(env.loading) {
      env.loading.dismiss();
      env.loading = null;
    }
  }

  alerta(mensaje: string) {
    let env = this;
    let titulo = 'Alerta';
    let boton = 'Aceptar';

    if(env.platform.is('cordova')) {
      env.dialogs.alert(mensaje, titulo, boton)
        .then(function () {
          console.log('Diálogo cerrado');
        })
        .catch(function (e) {
          console.log('Error al mostrar el diálogo', e);
        });
    } else {
      console.error(mensaje);
    }
  }

  buscarEnlace(item) {
    let env = this;
    env.enlacesFiltrados = env.enlaces.filter(function(elem) {
      return elem.nombre.toLowerCase().search(env.str_enlace.toLowerCase()) != -1;
    });
  }

  abrirEnlace(enlace) {
    window.open(enlace, '_system');
  }

  cancelarBusqueda(item) {
    let env = this;
    env.enlacesFiltrados = env.enlaces;
  }
}